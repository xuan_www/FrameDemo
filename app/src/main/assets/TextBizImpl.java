package com.manyiaby.presenter.textgo;

import android.util.Log;

import com.manyiaby.model.usermodel.MyaseUsers;
import com.manyiaby.presenter.utils.RetrofitInit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.RequestBody;

/**
 * Created by yx on 2017/6/9.
 * 简单请求结果返回
 * 观察者模式
 */

public class TextBizImpl {
    public void getLos(RequestBody requestBody) {
        RetrofitInit.getInstance()
                .create(TextBiz.class)
                .getLos(requestBody)
                .subscribeOn(Schedulers.io())   //  指定被观察者的操作在io线程中完成
                .observeOn(AndroidSchedulers.mainThread())  // 指定观察者接收到数据，然后在Main线程中完成
                .subscribe(new Consumer<MyaseUsers>() {
                    @Override
                    public void accept(@NonNull MyaseUsers myaseUsers) throws Exception {
                        Log.i("aaaa", "----------请求成功-------------");
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        Log.i("aaaa", "----------请求异常-------------");
                    }
                });

    }


}
