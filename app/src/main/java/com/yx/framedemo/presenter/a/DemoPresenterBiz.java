package com.yx.framedemo.presenter.a;

/**
 * 业务逻辑层接口
 * View层调用该接口获取数据
 */
public interface DemoPresenterBiz {
    void clearDisposable(); //  清空订阅对象，中断网络请求

    void getDemoInfo(String infos, String ids);

    void getDemoUsers();
}
