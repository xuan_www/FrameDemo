package com.yx.framedemo.presenter.a;


import com.yx.framedemo.model.a.DemoModelBean;
import com.yx.framedemo.model.utils.request.ModelApiImpl;
import com.yx.framedemo.model.utils.result.DataCallback;
import com.yx.framedemo.presenter.utils.JsonUtils;
import com.yx.framedemo.view.ui.a.AFragmentIFSView;
import com.yx.framedemo.view.ui.a.activity.DemoIFSView;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;

/**
 * 业务逻辑接口的实现
 * presenter负责所有逻辑处理
 */
public class DemoPresenterImpl implements DemoPresenterBiz {
    private DemoIFSView mView; //  控制View.通过这个接口P去跟View通信
    private AFragmentIFSView mView2; //  控制View - 如果Fragment中的Viewer接口被新的Activity界面注册，那么Fragment中的Viewer将失效

    private ModelApiImpl mModel; //  获取Model.通过它让P跟Model通信
    //  DemoPresenterImpl presenter = DemoPresenterImpl.getInstance();presenter.setmView2(this);
    private static DemoPresenterImpl instance;

    public static DemoPresenterImpl getInstance() {
        if (instance == null) {
            synchronized (DemoPresenterImpl.class) {
                if (instance == null) {
                    instance = new DemoPresenterImpl();
                }
            }
        }
        return instance;
    }

    public DemoPresenterImpl() {
        mModel = new ModelApiImpl();
    }

    public DemoPresenterImpl(DemoIFSView view) {
        this.mView = view;
        mModel = new ModelApiImpl();
    }

    public void setmView2(AFragmentIFSView mView2) {
        this.mView2 = mView2;
    }

    /**
     * 中断当前网络请求
     */
    @Override
    public void clearDisposable() {
        mModel.clearDisposable();
    }

    /**
     * 查询服务器 获取Demo信息
     *
     * @param infos 传的参数
     */
    @Override
    public void getDemoInfo(String infos, String ids) {
        JSONObject requests = new JSONObject();
        try {
            requests.put("infos", infos);
            requests.put("ids", ids);
            //  通过工具类请求并拿到Model
            mModel.getDemoInfo("Token", JsonUtils.inItRequestBody(requests), new DataCallback<DemoModelBean>() {
                @Override
                public void onSuccess(DemoModelBean bean) {
                    mView.onToast(bean.getName());
                    mView.onSetText(bean.getPhone());
                }

                @Override
                public void onFailed(String msg) {
                    mView2.onFail(msg);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void getDemoUsers() {
        try {
            RequestBody requestBody = JsonUtils.getInstance().addJsonObject(null);
            //  通过工具类请求并拿到Model
            mModel.getDemoUsers("Token", requestBody, new DataCallback<String>() {
                @Override
                public void onSuccess(String s) {
                    mView.onToast(s);
                    mView.onSetText(s);
                }

                @Override
                public void onFailed(String msg) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
