package com.yx.framedemo.presenter.utils;


import android.content.pm.PackageInfo;

import com.yx.framedemo.BuildConfig;
import com.yx.framedemo.view.main.ProxyApplication;
import com.yx.framedemo.view.utils.log.LogUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;


/**
 * Created by yx on 2017/6/14.
 * Json的拼接工具类
 * 请求数据前将数据拼接
 */

public class JsonUtils {
    private static JsonUtils jsonUtils;
    private JSONObject requestJsons; //  发送过去的Json
    private RequestBody requestBody; //  由发送Json封装的Body体

    /**
     * 单例 1
     * 缺点，创造速度略慢
     *
     * @return
     */
    public static JsonUtils inItJsonUtils() {
        if (jsonUtils == null) {
            synchronized (JsonUtils.class) {
                if (jsonUtils == null) {
                    jsonUtils = new JsonUtils();
                }
            }
        }
        return jsonUtils;
    }

    /**
     * 单例2，匿名静态内部类
     */
    private static class Holder { //  单例2帮助方法
        private static final JsonUtils jsonUtils = new JsonUtils();
    }

    public static JsonUtils getInstance() {
        return Holder.jsonUtils;
    }

    /**
     * 默认构造
     */
    private JsonUtils() {
        requestJsons = new JSONObject();
        defaultJsons();
    }

    /**
     * 默认JSON参数
     */
    private void defaultJsons() {
        try {
            requestJsons.put("device", "android." + ProxyApplication.getInstance().getmPhoneType()); //  手机名
            requestJsons.put("PhoneId", ProxyApplication.getInstance().getOnlyId()); //  唯一标识

            String userId = "";
            requestJsons.put("userId", (userId != null && !"".equals(userId)) ? userId : ""); //  当前登录用户ID
            //  当前版本号
            PackageInfo packageInfo = ProxyApplication.getInstance().getPackageManager().getPackageInfo(ProxyApplication.getInstance().getPackageName(), 0);
            String versionCode = BuildConfig.VERSION_NAME;
            requestJsons.put("versionCode", versionCode);
        } catch (Exception e) {
            LogUtil.iYx("默认的Json参数异常-JsonUtils.java");
            e.printStackTrace();
        }

    }

    /**
     * 请求Body体封装
     * 默认Json+传入Json 合体
     *
     * @param parameters 传入的各种请求参数，如果 无参请求 请传null
     * @return
     */
    public RequestBody addJsonObject(JSONObject parameters) {
        try {
            requestJsons.put("parameters", parameters == null ? "" : parameters.toString());
            // application/json:是JSON格式提交的一种识别方式。在请求头里标示。
            // multipart/form-data指表单数据有多部分构成,既有文本数据,又有文件等二进制数据的意思。”
            requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), requestJsons.toString());
            LogUtil.iYx("====参数拼接===  " + requestJsons.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestBody;
    }

    /**
     * 最终请求Body体返回
     *
     * @param requests
     * @return
     */
    public static RequestBody inItRequestBody(JSONObject requests) {
        return JsonUtils.getInstance().addJsonObject(requests);
    }


}
