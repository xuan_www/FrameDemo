package com.yx.framedemo.view.viewblock.welcome;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.yx.framedemo.R;

/**
 * Created by yx on 2017/6/1.
 */

public class ADActivity extends Activity implements ViewPager.OnPageChangeListener, View.OnClickListener {
    //  测试One
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private int[] imgs = new int[]{R.drawable.welcome_one, R.drawable.welcome_two, R.drawable.welcome_three, R.drawable.welcome_four};
    //   底部小点图片
    private ImageView[] dots;
    //   记录当前选中位置
    private int currentIndex;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_ada);
        viewPager = (ViewPager) findViewById(R.id.ada_vp);
        viewPagerAdapter = new ViewPagerAdapter(this, imgs); //   进行图片的适配
        viewPager.setAdapter(viewPagerAdapter);
        //   页面改变监听，手指滑动翻页触发
        viewPager.addOnPageChangeListener(this);
        //   初始化底部小点
        initDots();
    }


    private void initDots() {
        LinearLayout ll = (LinearLayout) findViewById(R.id.ada_ll); //   获得layout视图

        dots = new ImageView[imgs.length]; //   底部小点等于图片数的长度（4）

        //   循环取得小点图片
        for (int i = 0; i < imgs.length; i++) {
            dots[i] = (ImageView) ll.getChildAt(i); //   获得第i个点点图片
            dots[i].setEnabled(true); //   都设为灰色
            dots[i].setOnClickListener(this); //   设置点点的点击事件，需要implements
            //   OnClickListener
            dots[i].setTag(i); //   设置位置tag，方便取出与当前位置对应
        }

        currentIndex = 0; //   当前选中位置0
        dots[currentIndex].setEnabled(false); //   设置为白色，即选中状态
    }


    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        //   设置底部小点选中状态
        setCurDot(position);
    }

    @Override
    public void onClick(View v) {
        int position = (Integer) v.getTag();
        setCurView(position);
        setCurDot(position);
    }

    /**
     * 设置当前的引导页
     */
    private void setCurView(int position) {
        if (position < 0 || position >= imgs.length) {
            return;
        }
        viewPager.setCurrentItem(position);
    }

    /**
     * 这指当前引导小点的选中
     */
    private void setCurDot(int positon) {
        Log.i("aaaa", "======positon-is:=====" + positon);
        if (positon < 0 || positon > imgs.length - 1 || currentIndex == positon) {
            return;
        }
        Log.i("aaaa", "=======selected========" + positon);
        dots[positon].setEnabled(false);
        dots[currentIndex].setEnabled(true);
        currentIndex = positon;
    }

}
