package com.yx.framedemo.view.ui.b;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.yx.framedemo.R;
import com.yx.framedemo.view.utils.base.BaseFragment;
import com.yx.framedemo.view.utils.dialog.DialogLoadingUtil;
import com.yx.framedemo.view.utils.toast.ToastUtils;

public class BFragment extends BaseFragment {
    public static BFragment newInstance(String s) {
        BFragment bFragment = new BFragment();
        Bundle bundle = new Bundle();
        bundle.putString("str", s);
        bFragment.setArguments(bundle);
        return bFragment;
    }

    @Override
    public int initLayout() {
        return R.layout.fragment_b;
    }

    @Override
    public void initView(View view) {
        DialogLoadingUtil.showLoading(getContext(), false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ToastUtils.showToastApplication("OK");
                DialogLoadingUtil.hidn();
            }
        }, 2000);
        if (getArguments() != null) {
            String str = getArguments().getString("str");
        }
    }

}
