package com.yx.framedemo.view.utils.base;

import android.app.Activity;

import java.util.Stack;

/**
 * Created 2017/11/10.
 * APP统一管理
 */

public class AppManager {
    private Stack<Activity> activityStack;
    private volatile static AppManager instance;

    private AppManager() {
    }

    /**
     * 单一实例化当前对象
     */
    public static AppManager getAppManager() {
        if (instance == null) {
            synchronized (AppManager.class) {
                if (instance == null) {
                    instance = new AppManager();
                }
            }
        }
        return instance;
    }

    /**
     * 添加Activity到堆栈
     */
    public void addActivity(Activity activity) {
        if (activityStack == null) {
            activityStack = new Stack<>();
        }
        activityStack.add(activity);
    }

    /**
     * 获取当前Activity (堆栈中最后一个压入的)
     */
    public Activity currentActivity() {
        return activityStack.lastElement();
    }

    /**
     * 结束当前Activity(堆栈中最后一个压入的)
     * isFinishing() 用于判断 Activity 是否正在 finish。
     * isFinishing() 返回 true 后 isDestroy() 才会返回 true。
     */
    public void finishActivity() {
        //  finishActivity(currentActivity());
        Activity activity = currentActivity();
        if (activity != null && !activity.isFinishing()) {
            activityStack.remove(activity);
        }
    }

    /**
     * 结束指定的Activity
     * isFinishing() 用于判断 Activity 是否正在 finish。
     * isFinishing() 返回 true 后 isDestroy() 才会返回 true。
     *
     * @param activity 要结束的Activity
     */
    public void finishActivity(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            activityStack.remove(activity);
            activity.finish();
            activity = null;
        }
    }

    /**
     * 结束指定类名的Activity
     */
    public void finishActivity(Class<?> cls) {
        for (Activity activity : activityStack) {
            if (activity.getClass().equals(cls)) {
                finishActivity(activity);
                break;
            }
        }
    }

    /**
     * 结束所有Activity
     */
    private void finishAllActivity() {
        for (int i = 0; i < activityStack.size(); i++) {
            if (null != activityStack.get(i)) {
                activityStack.get(i).finish();
            }
        }
        activityStack.clear();
    }

    /**
     * 获取指定的Activity
     */
    public Activity getActivity(Class<?> cls) {
        if (activityStack != null) {
            for (Activity activity : activityStack) {
                if (activity.getClass().equals(cls)) {
                    return activity;
                }
            }
        }
        return null;
    }

    /**
     * 退出App
     */
    public void AppExit() {
        try {
            // 方法1：任务管理器结束所有后台进程，需添加权限<uses-permission android:name="android.permission.KILL_BACKGROUND_PROCESSES" /><!--任务管理器方法结束应用-->
            // ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            // am.killBackgroundProcesses(getPackageName());
            // 方法2：
            finishAllActivity();
            // 方法3：  myPid获取PID
            android.os.Process.killProcess(android.os.Process.myPid());
            // 方法4： 常规java、c#的标准退出法，返回值为0代表正常退出，避免自启需先结束所有Activity
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
