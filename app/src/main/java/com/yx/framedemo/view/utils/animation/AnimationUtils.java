package com.yx.framedemo.view.utils.animation;

import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;

/**
 * 动画
 */
public class AnimationUtils {

    /**
     * 缩放操作,放大1.5倍再还原
     *
     * @param mView
     */
    public static void scale(View mView) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mView, "scaleY", 1f, 1.2f, 1f);
        animator.setDuration(1000);
        animator.start();
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(mView, "scaleX", 1f, 1.2f, 1f);
        animatorX.setDuration(1000);
        animatorX.start();

        //          AnimatorSet animatorSet = new AnimatorSet(); //  组合动画
        //          ObjectAnimator scaleX = ObjectAnimator.ofFloat(mView, "scaleX", 1f,1.2f, 1f);
        //          ObjectAnimator scaleY = ObjectAnimator.ofFloat(mView, "scaleY", 1f,1.2f, 1f);
        //
        //          animatorSet.setDuration(1500);
        //          animatorSet.setInterpolator(new DecelerateInterpolator());
        //          animatorSet.play(scaleX).with(scaleY); //  两个动画同时开始
        //          animatorSet.start();

        //           //  第一个参数为 view对象，第二个参数为 动画改变的类型，第三，第四个参数依次是开始透明度和结束透明度。
        //          ObjectAnimator alpha = ObjectAnimator.ofFloat(mView, "alpha", 0f, 1f);
        //          alpha.setDuration(1500); //  设置动画时间
        //          alpha.setInterpolator(new DecelerateInterpolator()); //  设置动画插入器，减速
        //          alpha.setRepeatCount(0); //  设置动画重复次数，这里-1代表无限
        //          alpha.start(); //  启动动画。
    }


    public static void hiddenAnimation(View vTarget, long duration) {
        ViewWrapper wrapper = new ViewWrapper(vTarget);
        ObjectAnimator.ofInt(wrapper, "height", 0).setDuration(duration).start();
    }

    public static void showAnimation(View vTarget, int height, long duration) {
        ViewWrapper wrapper = new ViewWrapper(vTarget);
        ObjectAnimator.ofInt(wrapper, "height", height).setDuration(duration).start();
    }

    private static class ViewWrapper {
        private View mTarget;

        public ViewWrapper(View target) {
            mTarget = target;
        }

        public int getHeight() {
            return mTarget.getLayoutParams().height;
        }

        public void setHeight(int height) {
            mTarget.getLayoutParams().height = height;
            mTarget.requestLayout();
        }
    }


    /**
     * View左右抖动效果
     *
     * @param v
     */
    public static void goShake(View v) {
        TranslateAnimation animation = new TranslateAnimation(0, -5, 0, 0);
        animation.setInterpolator(new OvershootInterpolator());
        animation.setDuration(100);
        animation.setRepeatCount(2); //  表示重复次数
        animation.setRepeatMode(Animation.REVERSE); //  表示动画结束后,再执行,该方法有两种值， RESTART 和 REVERSE。 RESTART表示从头开始，REVERSE表示从末尾倒播。
        v.startAnimation(animation);
    }

    /**
     * View闪烁效果
     *
     * @param v
     */
    public static void goTwinkle(View v, long durationMillis) {
        //  闪烁
        AlphaAnimation animation = new AlphaAnimation(0.1f, 1.0f);
        animation.setDuration(durationMillis);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setFillAfter(true); //  动画执行完后是否停留在执行完的状态
        v.setAnimation(animation);
        animation.start();
    }

    /**
     * 旋转动画旋转90度
     *
     * @param v
     */
    public static void rotate(View v) {
        RotateAnimation animation = new RotateAnimation(0f, -90f, Animation.RELATIVE_TO_SELF,
                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(300); //  设置动画持续时间
        animation.setFillAfter(true); //  动画执行完后是否停留在执行完的状态
        v.setAnimation(animation);
        animation.start();
    }

    /**
     * 透明度渐变动画 由透明度0变化到透明度为1
     *
     * @param v
     */
    public static void alpha(View v, long durationMillis) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0, 1); //  初始化操作，参数传入0和1，即由透明度0变化到透明度为1
        alphaAnimation.setFillAfter(true); //  动画结束后保持状态
        alphaAnimation.setDuration(durationMillis); //  动画持续时间，单位为毫秒
        v.startAnimation(alphaAnimation); //  开始动画
    }

    /**
     * 透明度渐变动画 由透明度0变化到透明度为1
     *
     * @param v
     */
    public static void alphaNull(View v, long durationMillis) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1, 0); //  初始化操作，参数传入0和1，即由透明度0变化到透明度为1
        alphaAnimation.setFillAfter(true); //  动画结束后保持状态
        alphaAnimation.setDuration(durationMillis); //  动画持续时间，单位为毫秒
        v.startAnimation(alphaAnimation); //  开始动画
    }
}
