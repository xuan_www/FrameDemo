package com.yx.framedemo.view.viewblock.gesturelock.gesture;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.yx.framedemo.R;
import com.yx.framedemo.view.main.ProxyApplication;

/***
 * One: 创建手势密码---按钮提示
 *  【手势锁已封装成视图块/代码块（ViewBlock）】
 *  【手势锁已封装成视图块/代码块（ViewBlock）】
 *  【手势锁已封装成视图块/代码块（ViewBlock）】
 */
public class GuideGesturePasswordActivity extends Activity {

    private RelativeLayout rootView;
    private Button gesturepwd_guide_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gesturepassword_guide);
        rootView = (RelativeLayout) this.findViewById(R.id.rootView);
        gesturepwd_guide_btn = (Button) this.findViewById(R.id.gesturepwd_guide_btn);
        gesturepwd_guide_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //   TODO Auto-generated method stub
                toCreateGesturePwd();
            }
        });
    }

    private void toCreateGesturePwd() {
        ProxyApplication.getInstance().getLockPatternUtils().clearLock();
        Intent intent = new Intent(GuideGesturePasswordActivity.this,
                CreateGesturePasswordActivity.class);
        //   打开新的Activity
        startActivity(intent);
        finish();
    }

}
