package com.yx.framedemo.view.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.yx.framedemo.R;
import com.yx.framedemo.view.ui.a.AFragment;
import com.yx.framedemo.view.ui.b.BFragment;
import com.yx.framedemo.view.ui.c.CFragment;
import com.yx.framedemo.view.ui.d.DFragment;
import com.yx.framedemo.view.ui.e.EFragment;
import com.yx.framedemo.view.utils.animation.AnimationUtils;
import com.yx.framedemo.view.utils.log.LogUtil;
import com.yx.framedemo.view.utils.toast.ToastUtils;

public class MainActivity extends AppCompatActivity {
    //  菜单
    private LinearLayout tab_one_btn, tab_two_btn, tab_three_btn, tab_four_btn, tab_five_btn; //  菜单父布局
    private TextView tab_one_icon, tab_two_icon, tab_four_icon, tab_five_icon; //  菜单图标控件
    private TextView tab_one_tv, tab_two_tv, tab_three_tv, tab_four_tv, tab_five_tv; //  菜单标题控件


    //  Fragment管理器
    private FragmentManager mFragmentManager; //  要管理activity中的fragments，你就需要使用FragmentManager。通过getFragmentManager（）或getSupportFragmentManager（）获得
    //  记录当前视图
    private Fragment currentFragment; //   当前显示的Fragment
    private int currentPosition = 0; //  当前显示界面对应下标
    //  各个Fragment
    //  private HomeArticleFragment mHomeFragment; //  文章主页
    //  private HomeFragment mHomeFragment; //  一版主页
    private AFragment oneFragment; //  A
    private BFragment twoFragment; //  B
    private CFragment threeFragment; //  C
    private DFragment fourFragment; //  D
    private EFragment fiveFragment; //  E
    private int versionCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inItView(); //  1.获得底部控件，并添加点击事件
        inItFragmentManager(savedInstanceState); //  2.获得FragmentManager管理器，并设置视图
    }

    //  1.获得底部导航，添加点击事件
    private void inItView() {
        tab_one_btn = (LinearLayout) findViewById(R.id.tab_one_btn);
        tab_two_btn = (LinearLayout) findViewById(R.id.tab_two_btn);
        tab_three_btn = (LinearLayout) findViewById(R.id.tab_three_btn);
        tab_four_btn = (LinearLayout) findViewById(R.id.tab_four_btn);
        tab_five_btn = (LinearLayout) findViewById(R.id.tab_five_btn);

        tab_one_icon = (TextView) findViewById(R.id.tab_one_icon);
        tab_two_icon = (TextView) findViewById(R.id.tab_two_icon);
        tab_four_icon = (TextView) findViewById(R.id.tab_four_icon);
        tab_five_icon = (TextView) findViewById(R.id.tab_five_icon);

        tab_one_tv = (TextView) findViewById(R.id.tab_one_tv);
        tab_two_tv = (TextView) findViewById(R.id.tab_two_tv);
        tab_three_tv = (TextView) findViewById(R.id.tab_three_tv);
        tab_four_tv = (TextView) findViewById(R.id.tab_four_tv);
        tab_five_tv = (TextView) findViewById(R.id.tab_five_tv);

        tab_one_btn.setOnClickListener(clicks);
        tab_two_btn.setOnClickListener(clicks);
        tab_three_btn.setOnClickListener(clicks);
        tab_four_btn.setOnClickListener(clicks);
        tab_five_btn.setOnClickListener(clicks);

    }

    //  2.获得Fragment管理器，并设置默认视图
    private void inItFragmentManager(Bundle savedInstanceState) {
        mFragmentManager = getSupportFragmentManager(); //  获取FragmentManager

        //  设置视图
        if (savedInstanceState == null) {
            LogUtil.iYx("----------走默认视图-------------");
            //  启动APP时走默认视图
            onSelectedView(1);
        } else {
            // 死而复活
            LogUtil.iYx("----------走销毁后视图-------------");
            //  在被销毁后，通过发送并保存过来的数据选中对应视图
            currentFragment = mFragmentManager.getFragment(savedInstanceState, "currentFragment");
            currentPosition = savedInstanceState.getInt("position");
            // FragmentTransaction transaction = mFragmentManager.beginTransaction();
            /**
             *  replace 是先remove掉相同id的所有fragment，然后在add当前的这个fragment。
             *  官方的方法是使用replace()来替换Fragment，
             *  但是replace()的调用会导致Fragment的onCreteView()(准备绘制Fragment界面时调用)被调用，
             *  所以切换界面时会无法保存当前的状态.(这个操作可能造成重影)
             */
            // transaction.replace(R.id.act_main_container, currentFragment);
            // 提交了这个移除事务
            // transaction.commitAllowingStateLoss();
            onSelectedView(currentPosition);
        }
    }

    //  3.设置点击事件处理事项
    private View.OnClickListener clicks = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tab_one_btn: //  视图F1
                    onSelectedView(1);
                    break;
                case R.id.tab_two_btn: //  视图F2
                    onSelectedView(2);
                    break;
                case R.id.tab_three_btn: //  中路法师
                    //  onSelectedView(3);
                    onSelectedMID(view);
                    break;
                case R.id.tab_four_btn: //  视图F3
                    onSelectedView(4);
                    break;
                case R.id.tab_five_btn: //  视图F4
                    //  if (ProxyApplication.getInstance().getUserData() == null) {
                    //      LoginUtil.goLogin(MainActivity.this);
                    //      return;
                    //  }
                    onSelectedView(5);
                    break;

            }
        }
    };


    /**
     * 3.1选中选择布局
     *
     * @param position 选中Fragment下标
     */
    private void onSelectedView(int position) {
        //  初始图标颜色
        tab_one_icon.setBackgroundResource(R.drawable.pic_firstpage);
        tab_two_icon.setBackgroundResource(R.drawable.pic_artists);
        tab_four_icon.setBackgroundResource(R.drawable.pic_message);
        tab_five_icon.setBackgroundResource(R.drawable.pic_me);
        //  初始文字颜色
        tab_one_tv.setTextColor(ContextCompat.getColor(this, R.color.color_999999));
        tab_two_tv.setTextColor(ContextCompat.getColor(this, R.color.color_999999));
        tab_four_tv.setTextColor(ContextCompat.getColor(this, R.color.color_999999));
        tab_five_tv.setTextColor(ContextCompat.getColor(this, R.color.color_999999));

        //  初始事务
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        //  隐藏布局(注意不要使用remove方法移除,hide即可)
        if (oneFragment != null) {
            transaction.hide(oneFragment);
        }
        if (twoFragment != null) {
            transaction.hide(twoFragment);
        }
        if (threeFragment != null) {
            transaction.hide(threeFragment);
        }
        if (fourFragment != null) {
            transaction.hide(fourFragment);
        }
        if (fiveFragment != null) {
            transaction.hide(fiveFragment);
        }

        //  设置布局
        switch (position) {
            case 1: //  =========================== A ======================================
                tab_one_icon.setBackgroundResource(R.drawable.pic_firstpage_s); //  底部视图
                tab_one_tv.setTextColor(ContextCompat.getColor(this, R.color.color_000000));
                //  来个动画
                AnimationUtils.scale(tab_one_icon);
                AnimationUtils.scale(tab_one_tv);
                if (oneFragment == null) { //  对应布局
                    oneFragment = AFragment.newInstance("AFragment");
                    transaction.add(R.id.act_main_container, oneFragment);
                } else {
                    /*
                     *为解决两个Fragment都使用共享元素动画时出现切换页面错误的场景（AC有动画）
                     * A 切 C C打开详情有共享动画返回后 再切D，再切到A显示的C。
                     * replace：先删除fragmentmanager中所有已添加的fragment中,容器id与当前要添加的fragment的容器id相同的fragment；然后再添加当前fragment
                     * 由于添加的时候都是在一个LinearLayout 中，那么所有的 fragment的容器Id都是一样的；
                     * 得出结论：  replace 会删除LinearLayout中所有fragment  ，然后再添加传入fragment对象
                     * transaction.replace(R.id.act_main_container, oneFragment);
                     */
                    transaction.show(oneFragment);
                }
                currentFragment = oneFragment; //  保存当前视图防止销毁
                currentPosition = 1;
                break;
            case 2: //  =========================== B ======================================
                tab_two_icon.setBackgroundResource(R.drawable.pic_artists_s); //  底部视图
                tab_two_tv.setTextColor(ContextCompat.getColor(this, R.color.color_000000));
                //  来个动画
                AnimationUtils.scale(tab_two_icon);
                AnimationUtils.scale(tab_two_tv);
                if (twoFragment == null) { //  对应布局
                    twoFragment = BFragment.newInstance("BFragment");
                    transaction.add(R.id.act_main_container, twoFragment);
                } else {
                    transaction.show(twoFragment);
                }
                currentFragment = twoFragment; //  保存当前视图防止销毁
                currentPosition = 2;
                break;
            case 3:
                LogUtil.iYx("-----未开发-目前3号页是弹出菜单----");

                //  tab_three_icon.setBackgroundResource(R.drawable.pic_message_s); //  底部视图
                tab_three_tv.setTextColor(ContextCompat.getColor(this, R.color.color_000000));

                //  来个动画
                //  AnimationUtils.scale(tab_four_icon);
                AnimationUtils.scale(tab_three_tv);
                if (threeFragment == null) { //  对应布局
                    threeFragment = CFragment.newInstance("CFragment");
                    transaction.add(R.id.act_main_container, threeFragment);
                } else {
                    transaction.show(threeFragment);
                }
                currentFragment = threeFragment; //  保存当前视图防止销毁
                currentPosition = 3;
                break;
            case 4: //  =========================== D ======================================
                tab_four_icon.setBackgroundResource(R.drawable.pic_message_s); //  底部视图
                tab_four_tv.setTextColor(ContextCompat.getColor(this, R.color.color_000000));

                //  来个动画
                AnimationUtils.scale(tab_four_icon);
                AnimationUtils.scale(tab_four_tv);
                if (fourFragment == null) { //  对应布局
                    fourFragment = DFragment.newInstance("DFragment");
                    transaction.add(R.id.act_main_container, fourFragment);
                } else {
                    transaction.show(fourFragment);
                }
                currentFragment = fourFragment; //  保存当前视图防止销毁
                currentPosition = 4;
                break;
            case 5: //  =========================== E ======================================
                tab_five_icon.setBackgroundResource(R.drawable.pic_me_s); //  底部视图
                tab_five_tv.setTextColor(ContextCompat.getColor(this, R.color.color_000000));
                //  来个动画
                AnimationUtils.scale(tab_five_icon);
                AnimationUtils.scale(tab_five_tv);
                if (fiveFragment == null) { //  对应布局
                    fiveFragment = EFragment.newInstance("EFragment");
                    transaction.add(R.id.act_main_container, fiveFragment);
                } else {
                    transaction.show(fiveFragment);
                }
                currentFragment = fiveFragment; //  保存当前视图防止销毁
                currentPosition = 5;
                break;
            default:
                LogUtil.iYx("------------MainActivity_给定position错误-----------");
                break;
        }

        //   事务提交；
        /**
         * 当使用commit方法时，系统将进行状态判断，如果状态（mStateSaved）已经保存，将发生"Can not perform this action after onSaveInstanceState"错误。
         * 我们在通过调用getSupportFragmentManager().beginTransaction()来获取这个FragmentTransaction类的实例来管理这些操作，将他们存进由activity管理的back stack中，
         * 这样我们就可以进行fragment变化的回退操作，也可以 getSupportFragmentManager() 这样去获取FragmentTransaction类的实例
         * 用add(), remove(), replace()方法，把所有需要的变化加进去这些操作组成一个集合transaction，然后调用commit()方法，将这些变化应用。
         * 这个commit()方法，你只能在avtivity存储他的状态之前调用，也就是onSaveInstanceState(),在onSaveInstanceState()方法之后去调用commit()，就会抛出我们遇到的这个异常
         * 我们可以去用【commitAllowingStateLoss()】这个方法去代替commit()来解决
         */
        //   transaction.commit();
        transaction.commitAllowingStateLoss(); //  防止部分手机由可见到不可见时出现闪退

    }

    /**
     * 中路
     *
     * @param v
     */
    private void onSelectedMID(View v) {
        ToastUtils.showToastApplication("弹出选择框");
    }

    //  4.onSaveInstanceState

    /**
     * 4.在activity处于后台被系统销毁时,该activity的onSaveInstanceState就会被执行,除非该activity是被用户主动销毁的
     * 通过onSaveInstanceState发送一个消息并保存起来
     * 内存回收Activity时、跳到其他Activity、打开多任务窗口、使用Home回到主屏幕这些操作中也被调用。
     * *
     *
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //  状态持久化代码
        if (currentFragment != null) {
            outState.putInt("position", currentPosition);
            mFragmentManager.putFragment(outState, "currentFragment", currentFragment); //  在Activity中保存Fragment
        }
        //  Google对于这句话的解释是“Always call the superclass so it can save the view hierarchy state”，
        //  大概意思是“总是执行这句代码来调用父类去保存视图层的状态”。
        //   TODO Auto-generated method stub
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onBackPressed() {
        //  super.onBackPressed();
        //  回到桌面
        Intent home = new Intent(Intent.ACTION_MAIN);
        //  FLAG_ACTIVITY_SINGLE_TOP 与加载模式singleTop功能相同,FLAG_ACTIVITY_CLEAR_TOP 销毁目标Activity和它之上的所有Activity，重新创建目标Activity
        //  FLAG_ACTIVITY_NEW_TASK 首先会查找是否存在和被启动的Activity具有相同的亲和性的任务栈（即taskAffinity，注意同一个应用程序中的activity的亲和性相同），
        //   如果有，则直接把这个栈整体移动到前台，并保持栈中旧activity的顺序不变，然后被启动的Activity会被压入栈，
        //   如果没有，则新建一个栈来存放被启动的activity，注意，默认情况下同一个应用中的所有Activity拥有相同的关系(taskAffinity).
        home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        home.addCategory(Intent.CATEGORY_HOME);
        startActivity(home);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //  若MainActivity启动模式为android:alwaysRetainTaskState="true" android:launchMode="singleTask"
        //  在退出登录操作时，得通过观察者模式发送消息到MainActivity中，然后case "Logout": logout = true; finish(); break;
        //  先调用Main自身Finish方法以结束在其上的所有任务栈，
        //  而后在Main的super.onDestroy();后判断logout为true并启动LoginActivity
    }
}
