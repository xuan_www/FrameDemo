package com.yx.framedemo.view.utils.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.yx.framedemo.view.main.MainActivity;
import com.yx.framedemo.view.utils.log.LogUtil;


public abstract class BaseFragment extends Fragment {
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(initLayout(), container, false);
        initView(rootView);
        setViewPaddingTop(); //  沉浸后设置View与TOP的距离
        return rootView;
    }

    public abstract int initLayout();

    public abstract void initView(View view);


    /**
     * 设置布局距离上面的间距等于沉浸式状态栏高度
     */
    private void setViewPaddingTop() {
        if (getActivity() instanceof MainActivity) { //  只有MainActivity中的Fragment才需要单独设置距上边的距离

            if (rootView instanceof ViewGroup) {
                ViewGroup vp = (ViewGroup) rootView;
                View viewchild = vp.getChildAt(0);
                if (viewchild != null) { //  很多时候沉浸式颜色 = 第一个子View颜色
                    LogUtil.iYx("--该Fragment第一个子View的ID是:--" + viewchild.getId());
                    viewchild.setPadding(0, StatusBarUtil.statusBarHeight, 0, 0);
                } else { //  否则直接设置父布局距TOP间距
                    rootView.setPadding(0, StatusBarUtil.statusBarHeight, 0, 0);
                }
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
