package com.yx.framedemo.view.viewblock.welcome;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.viewpager.widget.PagerAdapter;

import com.yx.framedemo.R;
import com.yx.framedemo.view.main.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yx on 2017/6/2.
 */

public class ViewPagerAdapter extends PagerAdapter {
    private int[] imgs; //   显示的图片
    private List<View> views; //   存放显示图片id
    private LayoutInflater lif;
    private Context context;

    public ViewPagerAdapter(Context contexts, int[] imgss) {
        this.context = contexts;
        this.imgs = imgss;
        this.lif = LayoutInflater.from(context); //   加载布局
        views = new ArrayList<View>();

        for (int id : imgs) { //   循环这些图片
            View v = lif.inflate(R.layout.welcome_intobt, null); //   加载这个布局
            v.setBackgroundResource(id); //   给每个背景图加个按钮
            views.add(v); //   存到视图中
        }
    }

    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //   从ViewPager中移除( ViewGroup container)
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //   添加到viewpager( ViewGroup container)
        container.addView(views.get(position));
        if (position == views.size() - 1) { //   6
            //   转到最后一项的Button
            Button bt = (Button) views.get(position).findViewById(R.id.wl_bt);
            //   使button展示
            bt.setVisibility(View.VISIBLE);
            //   设置点击事件
            bt.setOnClickListener(clickLis);
        }
        return views.get(position);
    }

    private View.OnClickListener clickLis = new View.OnClickListener() {
        public void onClick(View v) {
            Intent in = new Intent(context, MainActivity.class);
            context.startActivity(in);
            SharedPreferences sp = context.getSharedPreferences("firstapp", Context.MODE_PRIVATE);
            //   获得编辑器
            Editor e = sp.edit();
            e.putBoolean("isfirst", false);
            e.commit();

            ((Activity) context).finish();
        }

        ;

    };


}
