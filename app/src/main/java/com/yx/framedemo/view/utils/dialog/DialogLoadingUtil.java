package com.yx.framedemo.view.utils.dialog;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.yx.framedemo.R;


/**
 * Created by Yx
 * Time: 2018/6/27
 * Details:显示加载中...（通过DialogUtil.showLoadin（this）提示加载中，通过hidn隐藏）
 */
public class DialogLoadingUtil {
    private static Dialog dialog;
    private static Context mContext;
    private static boolean isFinish = false; //  false表示点击手机返回键不关闭页面，只关闭dialogLoading

    /**
     * 显示加载等待
     *
     * @param context   上下文（如果isFinish为true，则必须为Activity）
     * @param isFinishs 点击手机返回键是否关闭当前界面（如数据正在加载，我不想看了点了手机返回键就关闭等待框及页面Activity）
     */
    public static void showLoading(Context context, boolean isFinishs) {
        mContext = context;
        isFinish = isFinishs;
        View v = LayoutInflater.from(context).inflate(
                R.layout.dialog_waiting_loading, null);
        ProgressBar pb;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            pb = (ProgressBar) v.findViewById(R.id.loading_60);
        } else {
            pb = (ProgressBar) v.findViewById(R.id.loading);
        }
        pb.setVisibility(View.VISIBLE);
        dialog = new AlertDialog.Builder(context, R.style.TranslucentNoBar).show();
        dialog.setOnKeyListener(xxx);
        //   触摸dialog外边时dialog是否消失
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(v);

        Window w = dialog.getWindow();
        WindowManager.LayoutParams lp = w.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics();  //   获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * 0.28);  //   宽度设置为屏幕宽度的0.6--,全宽为1，最低是0
        //  lp.height = (int) (d.heightPixels * 0.28);  //   高度设置为屏幕的0.6
        lp.height = (int) (d.widthPixels * 0.28);  //   高度设置为屏幕宽度的0.6
        //   popupWindow.setWidth(display.widthPixels * 9 / 10);
        //   popupWindow.setHeight(display.heightPixels * 9 / 10);
        //   lp.alpha = 0.3f;
        w.setAttributes(lp);
    }

    public static void hidn() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog.cancel();
            dialog = null;
        }
    }

    private static DialogInterface.OnKeyListener xxx = new DialogInterface.OnKeyListener() {
        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_BACK) { //   按返回键对话框消失，透明度恢复 点击了手机返回键
                    try {
                        hidn();
                        if (isFinish) {
                            ((Activity) mContext).finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
            return false;
        }
    };

}
