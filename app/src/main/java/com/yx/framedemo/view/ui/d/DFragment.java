package com.yx.framedemo.view.ui.d;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.yx.framedemo.R;
import com.yx.framedemo.view.utils.base.BaseFragment;
import com.yx.framedemo.view.utils.dialog.DialogLoadingUtil;
import com.yx.framedemo.view.utils.toast.ToastUtils;


public class DFragment extends BaseFragment {

    public static DFragment newInstance(String s) {
        DFragment dFragment = new DFragment();
        Bundle bundle = new Bundle();
        bundle.putString("str", s);
        dFragment.setArguments(bundle);
        return dFragment;
    }

    @Override
    public int initLayout() {
        return R.layout.fragment_d;
    }

    @Override
    public void initView(View view) {
        DialogLoadingUtil.showLoading(getContext(), false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ToastUtils.showToastApplication("OK");
                DialogLoadingUtil.hidn();
            }
        }, 2000);
    }


}
