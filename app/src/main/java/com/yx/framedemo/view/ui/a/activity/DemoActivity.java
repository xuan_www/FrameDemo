package com.yx.framedemo.view.ui.a.activity;

import android.Manifest;
import android.view.View;
import android.widget.TextView;

import com.tbruyelle.rxpermissions2.RxPermissions;
import com.yx.framedemo.R;
import com.yx.framedemo.presenter.a.DemoPresenterImpl;
import com.yx.framedemo.view.utils.base.BaseActivity;
import com.yx.framedemo.view.utils.log.LogUtil;
import com.yx.framedemo.view.utils.toast.ToastUtils;

import io.reactivex.functions.Consumer;

/**
 * 演示Demo
 * View负责显示
 * Yx
 */
public class DemoActivity extends BaseActivity implements DemoIFSView {
    private TextView text_tv;
    private DemoPresenterImpl testPresenter;

    @Override
    public int initLayout() {
        return R.layout.activity_demo;
    }

    @Override
    public void initView() {
        testPresenter = new DemoPresenterImpl(this);
        text_tv = (TextView) findViewById(R.id.text_tv);
        text_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ToastUtils.showToastApplication("加载数据中...");
                testPresenter.getDemoInfo("Demo", "IDs");
                testPresenter.getDemoUsers();
            }
        });

        //  权限使用示例,类名上面加上@SuppressLint("CheckResult")取消规范检查
        final RxPermissions rxPermissions = new RxPermissions(this);
        //  普通方式
        rxPermissions.request(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE, Manifest.permission.INTERNET).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) throws Exception {
                if (aBoolean) {
                    //  申请的权限全部允许
                    ToastUtils.showToastApplication("允许了权限!");
                } else {
                    //  只要有一个权限被拒绝，就会执行
                    ToastUtils.showToastApplication("拒绝授权!");
                }
            }
        });
        //  Lambda 方式
        rxPermissions
                .request(Manifest.permission.CAMERA)
                .subscribe(granted -> {
                    if (granted) {  //   Always true pre-M
                        //   I can control the camera now
                        ToastUtils.showToastApplication("允许了权限!");
                    } else {
                        //   Oups permission denied
                        ToastUtils.showToastApplication("拒绝授权!");
                    }
                });

    }

    @Override
    public void onToast(String str) {
        LogUtil.iYx(str);
        ToastUtils.showToastApplication(str);
    }

    @Override
    public void onSetText(String str) {
        text_tv.setText(str);
    }
}
