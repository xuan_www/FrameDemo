package com.yx.framedemo.view.viewblock.welcome;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.yx.framedemo.R;
import com.yx.framedemo.view.main.MainActivity;

/**
 * Created by yx on 2017/6/1.
 * <p>
 * 【欢迎页已封装成视图块/代码块（ViewBlock）】
 * 【欢迎页已封装成视图块/代码块（ViewBlock）】
 * 【欢迎页已封装成视图块/代码块（ViewBlock）】
 */
public class WelcomeActivity extends Activity {
    private Handler handler = new Handler();
    private Intent in = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_activity);

        SharedPreferences sp = getSharedPreferences("firstapp", MODE_PRIVATE);
        if (sp.getBoolean("isfirst", true)) {
            in = new Intent(this, ADActivity.class);
        } else {
            in = new Intent(this, MainActivity.class);
        }
        handler.postDelayed(r, 2500);
    }

    private Runnable r = new Runnable() {

        @Override
        public void run() {
            startActivity(in);
            finish(); //   关闭
            //   System.exit(1); //  关闭
            //   android.os.Process.killProcess(android.os.Process.myPid()); //  退出该运行的程序
        }
    };
}
