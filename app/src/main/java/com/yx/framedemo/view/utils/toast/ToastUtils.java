package com.yx.framedemo.view.utils.toast;

import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.widget.Toast;

import com.yx.framedemo.view.main.ProxyApplication;
import com.yx.framedemo.view.utils.log.LogUtil;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;


/**
 * Created by yx on 2017/6/2.
 * 文本提示Toast
 */

public class ToastUtils {

    private static Toast mToast;

    /**
     * 正常Toast
     *
     * @param text
     */
    public static void showToastApplication(String text) {

        if (mToast == null) {
            mToast = Toast.makeText(ProxyApplication.getInstance(), text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) { //  小于等于28/9.0系统则强制Toast,否则10.0正常Toast
            //  解决某些5.0以上关闭消息通知后 Toast无法显示问题
            try {
                //  1通过反射获取Toast的getService方法
                Method serviceMethod = Toast.class.getDeclaredMethod("getService");
                serviceMethod.setAccessible(true);
                //  2调用 toast 中的getService() 方法 返回INotificationManager类型的Object
                final Object iNotificationManagerObj = serviceMethod.invoke(mToast);
                //  3反射获取INotificationManager的Class
                Class iNotificationManagerCls = Class.forName("android.app.INotificationManager");
                //  4创建 INotificationManager的代理对象 替换Toast中的 INotificationManager
                final Object iNotificationManagerProxy = Proxy.newProxyInstance(mToast.getClass().getClassLoader(), new Class[]{iNotificationManagerCls}, new InvocationHandler() {
                            @Override
                            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                                //  强制使用系统Toast
                                if ("enqueueToast".equals(method.getName())
                                        || "enqueueToastEx".equals(method.getName())) {   //  华为p20 pro上为enqueueToastEx
                                    //  5上文中pkg 为“android”时为系统弹窗
                                    args[0] = "android";
                                }
                                LogUtil.iYx("强制使用系统Toast>>>>>>>>>");
                                return method.invoke(iNotificationManagerObj, args);
                            }
                        }
                );
                //  6进行替换
                Field sServiceFiled = Toast.class.getDeclaredField("sService");
                sServiceFiled.setAccessible(true);
                sServiceFiled.set(mToast, iNotificationManagerProxy);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mToast.show();

    }


    /**
     * 左上角Toast
     *
     * @param context
     * @param text
     */
    public static void showToastLeftTop(Context context, String text) {
        if (mToast == null) {
            mToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        mToast.setGravity(Gravity.TOP | Gravity.LEFT, 0, 0);
        mToast.show();
    }

    /**
     * 上部Toast
     *
     * @param context
     * @param text
     */
    public static void showToastTop(Context context, String text) {
        if (mToast == null) {
            mToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        //   如果你想往右边移动，将第二个参数设为>0；往下移动，增大第三个参数；
        mToast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 45);
        mToast.show();
    }

    /**
     * 屏幕中间Toast
     *
     * @param context
     * @param text
     */
    public static void showToastCenter(Context context, String text) {
        if (mToast == null) {
            mToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }

    public static void cancelToast() {
        if (mToast != null) {
            mToast.cancel();
        }
    }
}
