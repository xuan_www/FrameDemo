package com.yx.framedemo.view.ui.a;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.yx.framedemo.R;
import com.yx.framedemo.view.utils.base.BaseFragment;
import com.yx.framedemo.view.utils.dialog.DialogLoadingUtil;
import com.yx.framedemo.view.utils.toast.ToastUtils;


public class AFragment extends BaseFragment implements AFragmentIFSView {
    public static AFragment newInstance(String s) {
        AFragment aFragment = new AFragment();
        Bundle bundle = new Bundle();
        bundle.putString("str", s);
        aFragment.setArguments(bundle);
        return aFragment;
    }

    private TextView tv_left, tv_title;

    @Override
    public int initLayout() {
        return R.layout.fragment_a;
    }

    @Override
    public void initView(View view) {
        DialogLoadingUtil.showLoading(getContext(), false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ToastUtils.showToastApplication("OK");
                DialogLoadingUtil.hidn();
            }
        }, 2000);
        if (getArguments() != null) {
            String str = getArguments().getString("str");
        }

        tv_left = (TextView) view.findViewById(R.id.tv_left);
        tv_title = (TextView) view.findViewById(R.id.tv_title);
        tv_title.setText(R.string.app_name);
    }

    @Override
    public void onFail(String error) {

    }
}
