package com.yx.framedemo.view.ui.e;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.yx.framedemo.R;
import com.yx.framedemo.view.utils.base.BaseFragment;
import com.yx.framedemo.view.utils.dialog.DialogLoadingUtil;
import com.yx.framedemo.view.utils.toast.ToastUtils;

public class EFragment extends BaseFragment {
    public static EFragment newInstance(String s) {
        EFragment eFragment = new EFragment();
        Bundle bundle = new Bundle();
        bundle.putString("str", s);
        eFragment.setArguments(bundle);
        return eFragment;
    }

    @Override
    public int initLayout() {
        return R.layout.fragment_e;
    }

    @Override
    public void initView(View view) {
        DialogLoadingUtil.showLoading(getContext(), false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ToastUtils.showToastApplication("OK");
                DialogLoadingUtil.hidn();
            }
        }, 2000);
    }

}
