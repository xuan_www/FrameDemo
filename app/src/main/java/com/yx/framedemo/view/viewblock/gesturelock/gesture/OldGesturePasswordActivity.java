package com.yx.framedemo.view.viewblock.gesturelock.gesture;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.yx.framedemo.R;
import com.yx.framedemo.view.main.ProxyApplication;
import com.yx.framedemo.view.viewblock.gesturelock.view.LockPatternUtils;
import com.yx.framedemo.view.viewblock.gesturelock.view.LockPatternView;

import java.util.List;

/**
 * Created by My Love on 2015/11/25.
 * 请输入旧手势密码
 */
@SuppressLint("ResourceAsColor")
public class OldGesturePasswordActivity extends AppCompatActivity {

    private LockPatternView mLockPatternView;
    private int mFailedPatternAttemptsSinceLastTimeout = 0;
    private CountDownTimer mCountdownTimer = null;
    //   private Handler mHandler = new Handler();

    private Animation mShakeAnim;

    private TextView mTextView, to_cancel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gesturepassword_old);
        mTextView = (TextView) findViewById(R.id.gesturepwd_unlock_failtipb);
        to_cancel = (TextView) findViewById(R.id.to_cancel);
        to_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mLockPatternView = (LockPatternView) findViewById(R.id.gesturepwd_unlock_lockviewb);
        mLockPatternView.setOnPatternListener(mChooseNewLockPatternListener);
        mLockPatternView.setTactileFeedbackEnabled(true);
        mShakeAnim = AnimationUtils.loadAnimation(this, R.anim.gesture_shake_x);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCountdownTimer != null)
            mCountdownTimer.cancel();
    }

    private Runnable mClearPatternRunnable = new Runnable() {
        public void run() {
            mLockPatternView.clearPattern();
        }
    };

    protected LockPatternView.OnPatternListener mChooseNewLockPatternListener = new LockPatternView.OnPatternListener() {

        @Override
        public void onPatternStart() {
            //   TODO Auto-generated method stub
            mLockPatternView.removeCallbacks(mClearPatternRunnable);
            patternInProgress();
        }

        public void onPatternDetected(List<LockPatternView.Cell> pattern) {
            //   TODO Auto-generated method stub
            if (pattern == null)
                return;
            if (ProxyApplication.getInstance().getLockPatternUtils().checkPattern(pattern)) { //   解锁成功
                mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Correct);

                //   解锁成功跳转到创建手势密码页面----
                //   清除 手势文件
                ProxyApplication.getInstance().getLockPatternUtils().clearLock();
                UpdateGesturePassword();
            } else { //   解锁失败-----重新登录
                mLockPatternView
                        .setDisplayMode(LockPatternView.DisplayMode.Wrong);

                if (pattern.size() >= LockPatternUtils.MIN_PATTERN_REGISTER_FAIL) {

                    mFailedPatternAttemptsSinceLastTimeout++;
                    int retry = LockPatternUtils.FAILED_ATTEMPTS_BEFORE_TIMEOUT
                            - mFailedPatternAttemptsSinceLastTimeout;
                    if (retry > 0) {
                        //  changeUser.setVisibility(View.VISIBLE);
                        if (retry == 0)
                            Toast.makeText(OldGesturePasswordActivity.this, OldGesturePasswordActivity.this
                                    .getString(R.string.toastlock), Toast.LENGTH_SHORT).show();

                        mTextView.setText("密码错误，还可以再输入" + retry + "次");
                        //  mHeadTextView.setTextColor(Color.RED);
                        mTextView.startAnimation(mShakeAnim);
                    } else {
                        //   疯狂输入手势密码次数用完后：清空手势密码，清空本地文件，状态改为未登录
                        //  delete
                        //   清除 手势文件
                        ProxyApplication.getInstance().getLockPatternUtils().clearLock();
                    }
                } else {
                    Toast.makeText(OldGesturePasswordActivity.this, "输入长度不够，请重试", Toast.LENGTH_SHORT).show();
                }
                mLockPatternView.clearPattern();
            }
        }


        @Override
        public void onPatternCleared() {
            //   TODO Auto-generated method stub
            mLockPatternView.removeCallbacks(mClearPatternRunnable);
        }

        private void patternInProgress() {
        }

        @Override
        public void onPatternCellAdded(List<LockPatternView.Cell> pattern) {
            //   TODO Auto-generated method stub

        }


    };

    Runnable attemptLockout = new Runnable() {

        @Override
        public void run() {
            mLockPatternView.clearPattern();
            mLockPatternView.setEnabled(false);
            mCountdownTimer = new CountDownTimer(
                    LockPatternUtils.FAILED_ATTEMPT_TIMEOUT_MS + 1, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    int secondsRemaining = (int) (millisUntilFinished / 1000) - 1;
                    if (secondsRemaining > 0) {
                        mTextView.setText(secondsRemaining + " 秒后重试");
                    } else {
                        mTextView.setText(OldGesturePasswordActivity.this
                                .getString(R.string.gesture_drawPwd));
                        mTextView.setTextColor(Color.WHITE);
                    }

                }

                @Override
                public void onFinish() {
                    mLockPatternView.setEnabled(true);
                    mFailedPatternAttemptsSinceLastTimeout = 0;
                }
            }.start();
        }
    };

    /**
     * 旧手势密码确认成功
     */
    private void UpdateGesturePassword() {
        //  不在跳转到演示界面
        //  Intent i = new Intent(this, GuideGesturePasswordActivity.class);
        //  i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //  startActivity(i);
        //  AnimationUtil.finishActivityAnimation(OldGesturePasswordActivity.this); //  关闭
        ProxyApplication.getInstance().getLockPatternUtils().clearLock();
        Intent intent = new Intent(this, CreateGesturePasswordActivity.class);
        intent.putExtra("old", "onold");
        //   打开新的Activity
        startActivity(intent);
        finish();
    }

}
