package com.yx.framedemo.view.ui.c;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.yx.framedemo.R;
import com.yx.framedemo.view.utils.base.BaseFragment;
import com.yx.framedemo.view.utils.dialog.DialogLoadingUtil;
import com.yx.framedemo.view.utils.toast.ToastUtils;

public class CFragment extends BaseFragment {
    public static CFragment newInstance(String s) {
        CFragment cFragment = new CFragment();
        Bundle bundle = new Bundle();
        bundle.putString("str", s);
        cFragment.setArguments(bundle);
        return cFragment;
    }

    @Override
    public int initLayout() {
        return R.layout.fragment_c;
    }

    @Override
    public void initView(View view) {
        DialogLoadingUtil.showLoading(getContext(), false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ToastUtils.showToastApplication("OK");
                DialogLoadingUtil.hidn();
            }
        }, 2000);
    }

}
