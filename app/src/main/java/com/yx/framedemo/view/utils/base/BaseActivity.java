package com.yx.framedemo.view.utils.base;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.yx.framedemo.view.utils.log.LogUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  沉浸式状态栏
        transparentStatusBar();
        //  状态栏文字颜色,true为灰色，false为系统默认白(统一修改或在合适情况下调用修改)
        StatusBarUtil.setStatusTextColor(true, this);
        // 禁止截图和录屏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        // 添加到管理列表
        AppManager.getAppManager().addActivity(this);

        //  初始化
        setContentView(initLayout());
        initView();
        setViewPaddingTop(); //  沉浸后设置View与TOP的距离
    }

    public abstract int initLayout();

    public abstract void initView();

    /**
     * 沉浸式状态栏
     */
    public void transparentStatusBar() {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { //  大于等于5.0
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS); //   透明状态栏
            if (checkDeviceHasNavigationBar()) {
                //  有虚拟导航栏
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS); //  透明的状态栏和导航栏，必须API Level >= 21/5.0使用
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS); //   SDK21 清理状态栏半透明背景
                window.setStatusBarColor(Color.TRANSPARENT); //   SDK21 将状态栏背景设置为透明
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) { //  7.0 通过反射改变灰色状态栏背景为透明
            try {
                Class decorViewClazz = Class.forName("com.android.internal.policy.DecorView");
                Field field = decorViewClazz.getDeclaredField("mSemiTransparentStatusBarColor");
                field.setAccessible(true);
                field.setInt(getWindow().getDecorView(), Color.TRANSPARENT);   //  改为透明
            } catch (Exception e) {
            }

        }
    }

    /**
     * 获取是否存在虚拟导航栏NavigationBar
     * true 有  false 无
     */
    public boolean checkDeviceHasNavigationBar() {
        boolean hasNavigationBar = false;
        Resources rs = getResources();
        int id = rs.getIdentifier("config_showNavigationBar", "bool", "android");
        if (id > 0) {
            hasNavigationBar = rs.getBoolean(id);
        }
        try {
            Class systemPropertiesClass = Class.forName("android.os.SystemProperties");
            Method m = systemPropertiesClass.getMethod("get", String.class);
            String navBarOverride = (String) m.invoke(systemPropertiesClass, "qemu.hw.mainkeys");
            if ("1".equals(navBarOverride)) {
                hasNavigationBar = false;
            } else if ("0".equals(navBarOverride)) {
                hasNavigationBar = true;
            }
        } catch (Exception e) {
        }
        return hasNavigationBar;
    }


    /**
     * 设置布局距离上面的间距等于沉浸式状态栏高度
     */
    private void setViewPaddingTop() {
        //  设置布局距离上面的间距等于沉浸式状态栏高度
        //  if (!(this instanceof MainActivity)) { //  除MainActivity外（因为Main是在BaseFragment中设置与TOP距离）
        View nViews = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0); //  获取Activity布局最外层父布局
        if (nViews instanceof ViewGroup) {
            ViewGroup vp = (ViewGroup) nViews;
            View viewchild = vp.getChildAt(0);
            if (viewchild != null) { //  很多时候沉浸式颜色 = 第一个子View颜色
                LogUtil.iYx("--该Activity第一个子View的ID是:--" + viewchild.getId());
                viewchild.setPadding(0, StatusBarUtil.statusBarHeight, 0, 0);
            } else { //  否则直接设置父布局距TOP间距
                nViews.setPadding(0, StatusBarUtil.statusBarHeight, 0, 0);
            }
        }
        //  }
    }


    /**
     * 重写 getResource 方法，防止系统字体影响
     */
    @Override
    public Resources getResources() { // 禁止app字体大小跟随系统字体大小调节
        Resources resources = super.getResources();
        if (resources != null && resources.getConfiguration().fontScale != 1.0f) {
            Configuration configuration = resources.getConfiguration();
            configuration.fontScale = 1.0f; // 只设置字体默认大小,若要调节显示大小则sp改为dp
            // configuration.densityDpi = getDefaultDisplayDensity(); //恢复默认的[显示大小]
            // configuration.setToDefaults(); // 设置所有的内容都是默认
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }
        return resources;
    }


    /**
     * 获取手机出厂时默认的densityDpi
     */
    private int getDefaultDisplayDensity() {
        try {
            Class aClass = Class.forName("android.view.WindowManagerGlobal");
            Method method = aClass.getMethod("getWindowManagerService");
            method.setAccessible(true);
            Object iwm = method.invoke(aClass);
            Method getInitialDisplayDensity = iwm.getClass().getMethod("getInitialDisplayDensity", int.class);
            getInitialDisplayDensity.setAccessible(true);
            Object densityDpi = getInitialDisplayDensity.invoke(iwm, Display.DEFAULT_DISPLAY);
            return (int) densityDpi;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppManager.getAppManager().finishActivity();
    }

}
