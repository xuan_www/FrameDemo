package com.yx.framedemo.view.main;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import androidx.multidex.MultiDexApplication;

import com.yx.framedemo.view.utils.base.StatusBarUtil;
import com.yx.framedemo.view.utils.log.LogUtil;
import com.yx.framedemo.view.viewblock.gesturelock.view.LockPatternUtils;

import java.util.UUID;

/*
 *包名命名：com.个人或公司代码.应用代码
 */
public class ProxyApplication extends MultiDexApplication {
    //  1.懒汉模式单例，需要时才创建自己
    private static ProxyApplication instance;

    public static ProxyApplication getInstance() {
        if (instance == null) {
            synchronized (ProxyApplication.class) {
                if (instance == null) {
                    instance = new ProxyApplication();
                }
            }
        }
        return instance;
    }

    //  2.懒汉静态内部类;既实现了线程安全，又避免了同步带来的性能影响。
    private static class ApplicationHolder {
        private static final ProxyApplication instance = new ProxyApplication();
    }

    public static ProxyApplication getInstances() {
        return ApplicationHolder.instance;
    }

    //  3.饿汉模式单例,类创建时就存在
    private static ProxyApplication instances = new ProxyApplication();

    public static ProxyApplication getInstancess() {
        return instances;
    }

    //  以上单例选择2号性能最优


    //  --------------变量申明区↓--------------
    public static boolean isDebug = true; //  是否开启Debug模式
    private LockPatternUtils mLockPatternUtils; //   手势锁
    private String onlyId; //  手机唯一ID
    private String mPhoneType;  //   手机型号
    //  --------------变量申明区↑----------------


    @Override
    public void onCreate() {
        super.onCreate();
        if (isDebug) {
            LogUtil.LEVEL = LogUtil.verbose; //    VERBOSE 打开日志显示
        } else {
            LogUtil.LEVEL = LogUtil.nothing; //    NOTHING 关闭日志显示
        }
        //  初始化
        instance = this;
        StatusBarUtil.getStatusBarHeight2(this); //  初始状态栏高度
        LogUtil.iYx("---状态栏高度：---" + StatusBarUtil.statusBarHeight);

        getPhoneInfo(); //  获得IMEI
        if (onlyId == null || "".equals(onlyId)) {
            getUUID(); //  如果没有获得IMEI则使用UUID
        }

        mLockPatternUtils = new LockPatternUtils(this); //   手势锁

    }


    //  -----------------方法实现区↓------------

    /**
     * 手势锁工具类
     *
     * @return
     */
    public LockPatternUtils getLockPatternUtils() { //   手势锁
        return mLockPatternUtils;
    }


    /**
     * 获取手机信息
     */
    private void getPhoneInfo() {
        try {
            String mtype = android.os.Build.MODEL;  //   手机型号设备名称
            setmPhoneType(mtype); //  手机型号设备名称
            //  权限操作系列
            TelephonyManager mTm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            @SuppressLint("MissingPermission") String imei = mTm.getDeviceId(); //  IMEI是唯一的，它应该类似于 359881030314356（除非你有一个没有量产的手机（水货）它可能有无效的IMEI，如：0000000000000）。
            //  String imsi = mTm.getSubscriberId(); //  手机型号
            //  String numer = mTm.getLine1Number();  //   手机号码，有的可得，有的不可得
            setOnlyId(imei); //  唯一标识IMEI
        } catch (Exception e) {
            LogUtil.iYx("---高版本无法获取IMEI--");
        }

    }

    /**
     * 得到全局唯一UUID
     */
    private void getUUID() {
        String myUuid = ""; //  UUID
        SharedPreferences mShare = getSharedPreferences("myUuid", MODE_PRIVATE);
        if (mShare != null) {
            myUuid = mShare.getString("myUuid", "");
        }
        if (TextUtils.isEmpty(myUuid) || myUuid.equals("")) {
            myUuid = UUID.randomUUID().toString();
            mShare.edit().putString("mywuuid", myUuid).commit();
        }
        setOnlyId(myUuid);

    }
    //  ---------------方法实现区↑------------


    //  ---------------get√set方法↓------------

    /**
     * 手机唯一标识
     *
     * @return
     */
    public String getOnlyId() {
        return onlyId;
    }

    public void setOnlyId(String onlyId) {
        this.onlyId = onlyId;
    }

    /**
     * 手机型号
     *
     * @return
     */
    public String getmPhoneType() {
        return mPhoneType;
    }

    public void setmPhoneType(String mPhoneType) {
        this.mPhoneType = mPhoneType;
    }
}
