package com.yx.framedemo.view.ui.a.activity;

/**
 * 业务逻辑层接口Presenter与Activity/Fragment交互的桥梁
 * 在View中实现，在Presenter中调用
 */
public interface DemoIFSView {
    void onToast(String str); //  提示信息

    void onSetText(String str); //  设置文本信息
}
