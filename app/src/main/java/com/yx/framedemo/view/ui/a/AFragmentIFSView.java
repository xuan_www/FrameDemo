package com.yx.framedemo.view.ui.a;

/**
 * Creator:Yx   Time:2020/7/18 14:35
 * Description:AFragment接口，参数回调
 */
public interface AFragmentIFSView {

    void onFail(String error);

}
