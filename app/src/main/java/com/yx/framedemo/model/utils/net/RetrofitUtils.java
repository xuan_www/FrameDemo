package com.yx.framedemo.model.utils.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.yx.framedemo.model.utils.Urls;
import com.yx.framedemo.model.utils.request.RetrofitModelApi;
import com.yx.framedemo.view.main.ProxyApplication;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yx on 2017/6/2.
 */
public class RetrofitUtils {
    private Retrofit mRetrofit;
    public RetrofitModelApi apiService;

    /**
     * 单例
     */
    private static class Holder {
        // 此单例中，里面的所有内容只执行一次
        private static final RetrofitUtils mRetrofit = new RetrofitUtils();
    }

    public static RetrofitUtils getInstance() {
        return Holder.mRetrofit;
    }

    private RetrofitUtils() {
        mRetrofit = inItRetrofit(); //   Retrofit初始化
        apiService = mRetrofit.create(RetrofitModelApi.class); //   创建服务
    }

    /**
     * 初始化 Retrofit
     *
     * @return
     */
    private Retrofit inItRetrofit() {
        //  初始化OkHttp
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)     //  设置连接超时 20s
                .readTimeout(20, TimeUnit.SECONDS);      //  设置读取超时 20s
        try {
            builder.hostnameVerifier(SSLUtils.getHostnameVerifier()); // 校验名称,这个对象就是信任所有的主机,也就是信任所有https的请求，
            // 信任所有https的请求，相应的webview中需要设置setWebViewClient，onReceivedSslError，sslErrorHandler.proceed();
            builder.sslSocketFactory(SSLUtils.getSSLSocketFactory(), SSLUtils.getSSLX509TrustManager());  //  添加证书
        } catch (Exception e) {
            // 添加证书失败
            builder.sslSocketFactory(SSLUtils.getSSLSocketFactory()); //  添加证书
        }


        //  OkHttp日志拦截器
        if (ProxyApplication.isDebug) {  //   判断是否为DEBUG模式,也可通过:BuildConfig.DEBUG判断
            //   如果此时是在调试模式（debug）模式，则添加日志拦截器
            builder.addInterceptor(new Interceptors());
        }

        //   返回 Retrofit 对象
        return new Retrofit.Builder()
                .baseUrl(Urls.BASE) //  BASE_URL
                .client(builder.build())  //   传入请求客户端
                .addConverterFactory(GsonConverterFactory.create())  //   添加Gson转换工厂(若后端返回空数组如：image[]，则会被映射到为List,异常映射)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())  //   添加RxJava2调用适配工厂
                .build();


    }

    /**
     * 创建传入的某个服务接口
     */
    public <T> T create(Class<T> service) {
        //  判断服务接口是否为空
        checkNotNull(service, "service is null");
        return mRetrofit.create(service);
        //  return mRetrofit.create(LoginBiz.class); //  直接创建指定的某一个服务接口
    }

    /**
     * 判断服务接口是否为空
     *
     * @param object
     * @param message
     * @param <T>
     * @return
     */
    private <T> T checkNotNull(T object, String message) {
        if (object == null) {
            throw new NullPointerException(message);
        }
        return object;
    }


    private static Gson gson;

    /**
     * 增加后台返回""和"null"的处理
     * 1.int=>0
     * 2.double=>0.00
     * 3.long=>0L
     * 使用方法：.addConverterFactory(GsonConverterFactory.create(buildGson())) //  添加json转换框架
     *
     * @return
     */
    public static Gson buildGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .registerTypeAdapter(Integer.class, new DefaultAdapter.IntegerAdapter())
                    .registerTypeAdapter(int.class, new DefaultAdapter.IntegerAdapter())
                    .registerTypeAdapter(Double.class, new DefaultAdapter.DoubleAdapter())
                    .registerTypeAdapter(double.class, new DefaultAdapter.DoubleAdapter())
                    .registerTypeAdapter(Long.class, new DefaultAdapter.LongAdapter())
                    .registerTypeAdapter(long.class, new DefaultAdapter.LongAdapter())
                    .create();
        }
        return gson;
    }


    //   ------------------------------------OkHttp的一些配置↓需要时候可参考-----------------------------------------------
    //      OkHttpUtil.init(this)
    //              .setConnectTimeout(15) //  连接超时时间
    //          .setWriteTimeout(15) //  写超时时间
    //          .setReadTimeout(15) //  读超时时间
    //          .setMaxCacheSize(10 * 1024 * 1024) //  缓存空间大小
    //          .setCacheType(CacheType.FORCE_NETWORK) //  缓存类型
    //          .setHttpLogTAG("HttpLog") //  设置请求日志标识
    //          .setIsGzip(false) //  Gzip压缩，需要服务端支持
    //          .setShowHttpLog(true) //  显示请求日志
    //          .setShowLifecycleLog(false) //  显示Activity销毁日志
    //          .setRetryOnConnectionFailure(false) //  失败后不自动重连
    //          .setCachedDir(new File(cacheDir,"okHttp_cache")) //  缓存目录
    //              .setDownloadFileDir(downloadFileDir) //  文件下载保存目录
    //          .setResponseEncoding(Encoding.UTF_8) //  设置全局的服务器响应编码
    //          .addResultInterceptor(HttpInterceptor.ResultInterceptor) //  请求结果拦截器
    //          .addExceptionInterceptor(HttpInterceptor.ExceptionInterceptor) //  请求链路异常拦截器
    //          .setCookieJar(new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(this))) //  持久化cookie
    //              .build();

}
