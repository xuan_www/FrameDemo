package com.yx.framedemo.model.utils.net;

import com.yx.framedemo.view.utils.log.LogUtil;

import java.io.IOException;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by yx on 2017/6/2.
 * 调试模式请求拦截器
 */

public class Interceptors implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        //  这个chain里面包含了request和response，所以你要什么都可以从这里拿
        Request request = chain.request();
        long t1 = System.nanoTime(); //  请求发起的时间
        LogUtil.iYx("----发送请求----" + String.format("地址: %s on %s%n%s", request.url(), chain.connection(), request.headers()));

        Response response = chain.proceed(chain.request());
        long t2 = System.nanoTime(); //  收到响应的时间
        LogUtil.iYx("------耗时------" + String.format(Locale.getDefault(), "Received response for %s in %.1fms%n%s",
                response.request().url(), (t2 - t1) / 1e6d, response.headers()));
        //  这里不能直接使用response.body().string()的方式输出日志
        //  因为response.body().string()之后，response中的流会被关闭，程序会报错，我们需要创建出一
        //  个新的response给应用层处理
        MediaType mediaType = response.body().contentType();
        String content = response.body().string();
        LogUtil.iYx("----返回内容----Response Body:" + content);
        return response.newBuilder()
                .body(ResponseBody.create(mediaType, content))
                .build();
    }

}
