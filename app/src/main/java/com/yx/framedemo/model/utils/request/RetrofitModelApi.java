package com.yx.framedemo.model.utils.request;

import com.yx.framedemo.model.a.DemoModelBean;
import com.yx.framedemo.model.utils.result.HttpNoResult;
import com.yx.framedemo.model.utils.result.HttpResult;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * 所有接口集合，一一对应后端接口数
 * Rx写法 + Retrofit传值
 * Retrofit服务接口
 *
 * @author _Yx
 * Created by os on 2018/8/8.
 */
public interface RetrofitModelApi { //  所有接口定义均放在这里↓

    //  DemoInfo接口，获取Demo信息。
    @POST("demo_Info.action")
    //  接口方法名/.action/.do/不带点后缀 都行
    Observable<HttpResult<DemoModelBean>> getDemoInfos(@Header("Authorization") String tokenkey, @Body RequestBody route);

    //  DemoInfo接口，获取Demo信息。
    @POST("demo_User.action")
    //  接口方法名/.action/.do/不带点后缀 都行
    Observable<HttpNoResult> getDemoUsers(@Header("Authorization") String tokenkey, @Body RequestBody route);

    // 提交表单
    @Headers({"Content-Type: application/x-www-form-urlencoded; charset=utf-8"})
    // 或多个键值对：{key:val1,key:val2}
    @FormUrlEncoded
    @POST("bpapp/manager/data/batchImportData")
    Observable<HttpNoResult> shareLanXinss(@FieldMap Map<String, Object> map);

}
