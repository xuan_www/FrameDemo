package com.yx.framedemo.model.utils.result;

/**
 * Created by yx on 2017/10/11.
 */

public interface DataCallback<T> {
    void onSuccess(T t);

    void onFailed(String msg);
}
