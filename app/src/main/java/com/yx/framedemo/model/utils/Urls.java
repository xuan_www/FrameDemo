package com.yx.framedemo.model.utils;

/**
 * 网络请求地址
 */
public class Urls {
    //  public static final String BASE = "http: //  www.baidu.cn/service/"; //  内网测试地址Java Server
    public static final String BASE = "http://www.baidu.com/"; //  外网发布正式地址Java Server

    //  其他/第三方HTTP地址
    public static final String BASE_WX = "https://api.weixin.qq.com/sns/oauth2/"; //  微信地址
    public static final String BASE_SHARE = "http://app.manyiaby.com/ArtWork/Index?artWorkId="; //  分享地址

}
