package com.yx.framedemo.model.a;

import com.yx.framedemo.model.utils.net.RetrofitUtils;

/**
 * Creator:Yx
 * Time:2020/7/29 11:42
 * Description:提供给Presenter 调用
 * DemoModelImpl impl = new DemoModelImpl();
 * impl.getDemoInfo(1,2,3);
 */
public class DemoModelImpl {

    //  获取数据
    public void getDemoInfo(final String tokenkey, String uid) {
        RetrofitUtils.getInstance().apiService
                .getDemoInfos(tokenkey, null);
    }

}