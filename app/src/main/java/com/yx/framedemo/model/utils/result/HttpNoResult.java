package com.yx.framedemo.model.utils.result;

/**
 * Created by yx on 2017/6/14.
 * 返回结果解析公共参数类；无Data/Body体的
 * 格式：Observable< HttpNoResult >
 */
public class HttpNoResult {
    private String action;
    private String status;
    private String msg;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
