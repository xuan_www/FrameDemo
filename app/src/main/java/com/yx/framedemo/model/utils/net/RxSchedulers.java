package com.yx.framedemo.model.utils.net;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 对观察者
 * 以及 指定在UI线程等
 * 的代码提取
 */
@SuppressWarnings("unchecked")
public class RxSchedulers {

    /**
     * Lambda表达式写法
     *
     * @param <T>
     * @return
     */
    public static <T> ObservableTransformer<T, T> io_main() {
        return upstream ->
                upstream.subscribeOn(Schedulers.io())    //    指定被观察者的操作在io线程中完成
                        .observeOn(AndroidSchedulers.mainThread());  //   指定观察者接收到数据，然后在Main线程中完成
    }


    /**
     * 看不懂的
     * 这个是正常返回方法↓
     *
     * @param <T>
     * @return
     */
    public static <T> ObservableTransformer<T, T> io_mains() {
        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource apply(Observable upstream) {
                return upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
            }
        };
    }


    /**
     * Lambda表达式粗分析
     * Creat _Yx
     * 首先 Lambda表达式记住 左边是参数，右边是方法体
     * 先看   a => a + 10    ;   左边a是我们输入的一个参数a , =>转到、成为，输出a + 10;
     * 例：void ff( int a){ int xx = a + 10 }; Lambda: a => a+10
     * 例：void ff(List< int > a){
     *      List< int >  p = a;
     *       //  查询出所有大于6的数据
     *      List< int >  b = new ArrayList<  >
     *      for(int i =0;i< p.size():i++){
     *          if(p.get(i) > 6) {
     *              b.add(  p.get(i)  );
     *          }
     *      }
     * }
     * Lambda:void ff(List< int > a){
     *      List< int >  p = a;
     *      List< int >  b = p .stream().filter(s -> s > 6).collect(Collectors.toList()); //  查询出所有大于6的数据
     *
     *      b.stream().forEach(   int ->{ System.out.println(int + ""); }     ); //  遍历出来
     * }
     *
     *
     *   ->  指向并返回
     * 1. 不需要参数,返回值为 5
     *     () -> 5
     * 2. 接收一个参数(数字类型),返回其2倍的值
     *      x -> 2 * x
     * 3. 接受2个参数(数字),并返回他们的差值
     *      (x, y) -> x – y
     * 4. 接收2个int型整数,返回他们的和
     *      (int x, int y) -> x + y
     * 5. 接受一个 string 对象,并在控制台打印,不返回任何值(看起来像是返回void)
     *      (String s) -> System.out.print(s)
     *
     *  以前的循环方式
     *  for (String player : players) {
     *      System.out.print(player + "; ");
     *  }
     *   使用 lambda 表达式以及函数操作(functional operation)
     *   players.forEach((player) -> System.out.print(player + "; "));
     *
     *
     *
     */


}
