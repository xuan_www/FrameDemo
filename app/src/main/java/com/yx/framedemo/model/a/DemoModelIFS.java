package com.yx.framedemo.model.a;

import com.yx.framedemo.model.utils.result.HttpNoResult;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Creator:Yx   Time:2020/7/18 14:19
 * Description:本来每个Model都应该与自己的接口，去实现并且发送数据请求
 * 但为了好管理，则使用了Model-utils中的ModelIFSApiImpl去实现所有请求
 */
public interface DemoModelIFS {

    //  DemoInfo接口，获取Demo信息。
    @POST("demo_User.action")
    //  接口方法名/.action/.do/不带点后缀 都行
    Observable<HttpNoResult> getDemoUsers(@Header("Authorization") String tokenkey, @Body RequestBody route);

}
