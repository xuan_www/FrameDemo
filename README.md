# FrameDemo

#### 项目介绍
RxJava+RxAndroid+Retrofit+OkHttp+Mvp+模块化

模块化说明:

View 派生main，MianActivity 派生各个导航，对应的a b c d e...等等view文件夹  相应文件中对应的是底部导航相应的Fragment; 谁负责的模块写到谁的模块里；模块之间互相跳转则通过View下面的共有工具类去进行调用！这样实现模块化。互不影响。


#### 软件架构
软件架构说明

Model中有：utilsm是model中的工具类（对Retrofit 、Rx的封装；请求接口定义、实现；服务地址Url控制；数据回调接口）

Presenter中有：utilsp是presenter的工具类、

View中有：custom:自定义view文件夹、
utilsv 是view的工具类、
viewblock是一些提取封装好的代码块方便移植（比如一些可以模块化的功能，如手势密码、欢迎页模块等等，
一些功能模块都可以提出来一小块）
mycustom 文件夹是用来放所有的自定义view的



#### 使用说明

1. 参考
2. 参考
3. 参考

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)